<?php

$html = json_decode($_POST["divstring"]);
$width = json_decode($_POST["containerWidth"]);

$htmlOutput = fopen("wireframe.html", "w");

$body = "<body style='display:flex; flex-wrap:wrap; justify-content:center;'>\n";
fwrite($htmlOutput, $body);

$container = "<div class='container' style='display:flex; flex-wrap:wrap; justify-content:space-between; width:" . $width . ";'>\n";
fwrite($htmlOutput, $container);

for ($i = count($html) - 1; $i >= 0; $i--) {
    $div = "<div class='" . $html[$i][0] . "' style='width:". $html[$i][1] . "; height:" . $html[$i][2] . "; border:5px solid black; margin-bottom:30px;'> </div>\n";
    fwrite($htmlOutput, $div);
}

$container_end = "</div>";
fwrite($htmlOutput, $container_end);

$body_end = "</body>";
fwrite($htmlOutput, $body_end);

fclose($htmlOutput);



?>