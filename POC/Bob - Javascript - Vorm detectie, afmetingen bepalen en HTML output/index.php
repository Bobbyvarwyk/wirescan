<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>WireScan</title>
</head>

<body style="display:flex; flex-wrap:wrap; justify-content:center">
  <div class="inputoutput">
    <img style="display:none" id="imageSrc" src="img/Wireframe.jpeg" alt="No Image" />
  </div>
  <div class="inputoutput">
    <canvas id="canvasOutput" style="max-width:1500px;"></canvas>
  </div>

  <div class="html-output" id="html-output" style="display:flex; flex-wrap:wrap; justify-content:space-between; width:96%;">
  </div>


<script src="js/main.js"> </script>
<script async src="https://docs.opencv.org/master/opencv.js" type="text/javascript"></script>
</body>
</html>