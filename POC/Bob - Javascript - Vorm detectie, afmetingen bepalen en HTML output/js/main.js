let imgElement = document.getElementById('imageSrc');

var htmlList = []

window.onload = function() {
 
  let imageSource = cv.imread(imgElement);
  // let resizedImage = new cv.Mat();
  // let imageSize = new cv.Size(400, 566);

  // Resize the image so it's better readable.
  // cv.resize(imageSource, resizedImage, imageSize, 0, 0, cv.INTER_AREA);

  let gray = new cv.Mat();
  cv.cvtColor(imageSource, gray, cv.COLOR_RGBA2GRAY, 0);

  let tresholded = new cv.Mat();
  cv.threshold(gray, tresholded, 60, 255, cv.THRESH_BINARY);

  let edged = new cv.Mat();
  cv.Canny(tresholded, edged, 50, 100);

  let dilated = new cv.Mat();
  let M = cv.Mat.ones(5, 5, cv.CV_8U);
  let anchor = new cv.Point(-1, -1);
  cv.dilate(edged, dilated, M, anchor, iterations=1);

  let eroded = new cv.Mat();
  let E = cv.Mat.ones(5, 5, cv.CV_8U);
  let E_Anchor = new cv.Point(-1, -1);
  cv.erode(dilated, eroded, E, E_Anchor, iterations=1);

  let contours = new cv.MatVector();
  let hierarchy = new cv.Mat();
  cv.findContours(eroded, contours, hierarchy, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE);

  getShape(contours);
  
  // Show the image
  cv.imshow('canvasOutput', edged);
};

function getShape(c) {
  for (let x = 0; x < c.size(); x++) {
    let shape = "Null";
    let tmp = new cv.Mat();
    let cnt = c.get(x);
    let perim = cv.arcLength(cnt, false);
    cv.approxPolyDP(cnt, tmp, 0.04 * perim, true);
    let rect = cv.boundingRect(cnt);

    if (tmp.rows === 3) {
      shape = "Triangle"
    } else if(tmp.rows === 4) {
      if (rect.width == rect.height) {
          shape = "square";
      } else {
          shape = "rectangle";
      }
    } else if (tmp.rows === 5) {
      shape = "Pentagon";
    } else {
      shape = "circle";
    }

    htmlList.push([shape, rect.width, rect.height]);
  }
  console.log(htmlList);
  generateHTML();
}

function generateHTML() {

  var parent = document.getElementById("html-output");

  for (var counter = htmlList.length - 1; counter >= 0; counter--) {
     // var htmlOutput = '<div class="' + htmlList[counter][0] + '" style="width: ' + htmlList[counter][1] +  'px; height: ' + htmlList[counter][2] + 'px;"';
     
     var node = document.createElement("div");
     node.className = htmlList[counter][0];
     node.style.width = htmlList[counter][1] + "px";
     node.style.height = htmlList[counter][2] + "px";
     node.style.border = "3px solid black";
     node.style.margin = "0 0 30px 0";

     var outerWidth = Math.max(htmlList[counter][1]);

     parent.appendChild(node);

  }

  parent.style.width = outerWidth + 6 + "px";
}

console.log(Screen.width);