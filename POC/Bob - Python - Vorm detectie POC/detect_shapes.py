# Importeren van alle nodige modules en classes
from pyimagesearch.shapedetector import ShapeDetector
from pyimagesearch.drawcontour import DrawContour
import numpy as np
import argparse
import imutils
import cv2

# Zet de command line constructor op
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="path to the input image")
args = vars(ap.parse_args())

# Afbeelding inladen en verkleinen zodat vormen beter en sneller laden.
image = cv2.imread(args["image"])

resized = imutils.resize(image, width=300)
ratio = image.shape[0] / float(resized.shape[0])

# Zet de afbeelding om naar grijstinten en blur de afbeelding.
gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(gray, (5, 5), 0)
thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]

# Vind de contouren van de afbeelding en roep de 2 classes aan
# die ervoor zorgen dat alles juist wordt gescant & getekent. 
cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
sd = ShapeDetector()
dc = DrawContour()

# Loop door alle contouren van de afbeelding
for (i, c) in enumerate(cnts):

	dc.draw(image, c, i, ratio)
cv2.imshow("Image", image)

cv2.waitKey(0)
   

