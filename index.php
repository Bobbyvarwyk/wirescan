<html>
<head>
	<meta charset="Utf-8"> 
	<link rel="stylesheet" type="text/css" href="styles.css" />
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
</head>

<body>
	<div class="dashboard">
	<img style="display:none" id="imageSrc" src="img/test2.png" alt="No Image" />
		<div class="left">
			<div id="video"><img id="imageSrcOutput" src="img/test2.png" alt="No Image" /> </div>
		</div>
		<div class="right">
			<div class="inner-content">

				<div class="upload">
					<h1> Upload your wireframe </h1>
					<p> Upload your wireframe to our service </p>
					<input type='file' onchange="readImage(this);" />
				</div>

				<div class="controls">
					<h1> Wireframe to HTML </h1>
					<p> Once you uploaded your file, click on the scan button to generate your HTML </p>
					<button class="scanner" onclick="scanImage()" id="scanner" type="submit" name="submit"> Scan your wireframe </button>
					
				</div>

				<i class="fas fa-cog" id="cog"></i>

				<div class="finished" id="finish">
					<h1> HTML Generated </h1>
					<p> Once your HTML file is finished, you can download it by clicking the button below. </p>
					<a href="wireframe.html" download class="download"> Download HTML layout </a>
				</div>

			</div>
		</div>
	</div>

	<div class="html-output" id="html-output" style="display:flex; flex-wrap:wrap; justify-content:space-between; width:96%;">	</div>

</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script async src="https://docs.opencv.org/master/opencv.js" type="text/javascript"></script>
<script src="js/main.js"></script>
<script src="js/wirescan.js"></script>
</html>
