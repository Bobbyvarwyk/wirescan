// Haal het img element op dat het systeem moet uitlezen.
let imgElement = document.getElementById('imageSrc');

// maak een lege array aan om de gescande HTML elementen in op te slaan.
var htmlList = [];

// Lees de image uit via OpenCV, convert deze naar o.a grijswaarden, etc.. en zoek de contouren binnen de afbeelding.
function scanImage() {

  let imageSource = cv.imread(imgElement);

  // Convert de afbeelding verschillende keren om deze beter leesbaar te maken. We roepen hiervoor de convertImage functie aan.
  let gray = convertImage(imageSource, "gray");
  let treshold = convertImage(gray, "treshold");
  let edged = convertImage(treshold, "edge");
  let dilated = convertImage(edged, "dilate");
  let eroded = convertImage(dilated, "erode");

  // Vind de contouren binnen de afbeelding
  let contours = new cv.MatVector();
  let hierarchy = new cv.Mat();
  cv.findContours(eroded, contours, hierarchy, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE);

  // Roep de getShape functie aan om de vormen te detecteren.
  detectShapes(contours);   
};

// Deze functie convert de afbeelding naar het gewenste type, afhankelijk van het type dat hij mee krijgt.
function convertImage(image, type) {

  let convertedImage = new cv.Mat();

  let M = cv.Mat.ones(5, 5, cv.CV_8U);
  let anchor = new cv.Point(-1, -1);
  
  // Check welk type het is een koppel hier de juiste convert functie aan.
  switch(type) {
    case "gray":
      cv.cvtColor(image, convertedImage, cv.COLOR_RGBA2GRAY, 0);
    break;
    case "treshold":
      cv.threshold(image, convertedImage, 60, 255, cv.THRESH_BINARY);
    break;
    case "edge":
      cv.Canny(image, convertedImage, 50, 100);
    break;
    case "dilate":
      cv.dilate(image, convertedImage, M, anchor, iterations=1);
    break;
    case "erode":
      cv.erode(image, convertedImage, M, anchor, iterations=1);
    break;
  } 
  // Return convertedImage zodat de scanImage functie deze kan gebruiken.
  return convertedImage;
}

// Detecteer de vormen binnen de contours via de onderstaande functies.
function detectShapes(c) {

  for (let x = 0; x < c.size(); x++) {
    // Creer eerst een aantal globale variabelen die nodig zijn om de vorm te bepalen.
    let shape = "Null";
    let tmp = new cv.Mat();
    let cnt = c.get(x);
    let perim = cv.arcLength(cnt, false);
    cv.approxPolyDP(cnt, tmp, 0.04 * perim, true);
    let rect = cv.boundingRect(cnt);

    // Check hoeveel hoeken de vorm heeft, bepaal via een switch welke vorm het is.
    switch(tmp.rows) {
      case 3: // 3 hoeken
        shape = "Triangle"
      break;
      case 4: // 4 hoeken
        if(rect.width == rect.height) {
          shape = "square"
        } else {
          shape = "rectangle";
        }
      break;
      case 5: // 5 hoeken
        shape = "Pentagon";
      break;
      default: // als het object niet 3, 4 of 5 hoeken heeft is het een cirkel.
        shape = "circle";
      break;
    }
    htmlList.push([shape, rect.width, rect.height]); // Push de vorm en de breedte en hoogte naar de htmlList array.
  }
  createHTMLFile(); // Roep de createHTMLFile functie aan om de html file te maken.
}

function createHTMLFile() {

  // bepaal de grootste breedte in de array, geef deze mee als breedte van de overkoepelende div
  for (var counter = htmlList.length - 1; counter >= 0; counter--) {
    var containerWidth = Math.max(htmlList[counter][1]) + "px";
  }

  // Verzend een ajax request naar de createHTMl.php, deze maakt de HTML code aan.
  $.ajax({
    type: "POST",
    url: "createHTML.php",
    data: {"divstring" : JSON.stringify(htmlList), "containerWidth" : JSON.stringify(containerWidth)},
    dataType: "json",
    success: function (response) {
      console.log(response);
    },
    error: function(response) {
      console.log(response);
    }
  });

  // Leeg de HTML list na het verzenden, dit om nogmaals een wireframe in te laden.
  htmlList = [];
  showLoadIcon();
   
}

// ############################ Algemene functies ##################################

// Lees de geuploade afbeelding uit en plaats deze direct als preview in de #imageSrcOutput div
function readImage(input) {
  if(input.files && input.files[0]) {

    var reader = new FileReader();
    reader.onload = function(e) {
      $('#imageSrcOutput').attr('src', e.target.result);
      $('#imageSrc').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

// Laat het laad icoon zien.
function showLoadIcon() {
  var element = document.getElementById("cog");
  element.style.display = "inline-block";
  setTimeout(showFinished, 3000);
}

// Laat de download knop zien.
function showFinished() {
  var element = document.getElementById("finish");
  element.style.display = "inline-block";
  var element = document.getElementById("cog");
  element.style.display = "none";
}